QT += qml quick

CONFIG += c++11

INCLUDEPATH += /mnt/second_drive/Android/libs/build/include
LIBS += -L/mnt/second_drive/Android/libs/build/lib
CONFIG += static

SOURCES += main.cpp \
    ../DaiModern/sectiontype.cpp

HEADERS += ../DaiModern/sectiontype.h

OTHER_FILES += ../DaiModern/main.qml

INCLUDEPATH += ../DaiModern

LIBS += -lDai -lprotobuf

RESOURCES += ../DaiModern/main.qrc
QML_IMPORT_PATH = ../DaiModern

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
