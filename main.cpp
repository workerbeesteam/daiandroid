#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QSettings>

#include "sectiontype.h"
#include "quickparam.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setApplicationName("DaiAndroid");
    QCoreApplication::setOrganizationName("DeviceAccess.ru");
    QCoreApplication::setOrganizationDomain("DeviceAccess.ru");
    QCoreApplication::setApplicationVersion("1.0");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    qputenv("QT_LABS_CONTROLS_STYLE", "Material");

    qmlRegisterType<Dai::SectionType>("Dai", 1, 0, "SectionType");
    qmlRegisterType<Dai::QuickTemperatureParam>("Dai", 1, 0, "QuickTemperatureParam");
    qmlRegisterType<Dai::QuickLightingParam>("Dai", 1, 0, "QuickLightingParam");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
